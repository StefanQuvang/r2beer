/*
 * OnBoardLed.cpp
 *
 *  Created on: 30. aug. 2020
 *      Author: stefa
 */

#include <Driver/OnBoardLed.h>
#include "HAL/TM4C123G/GPIO/DigitalGPIO.h"
#include "TypeDef.h"

Config RedLed;
Config BlueLed;
Config GreenLed;

void mInitRed()
{
    RedLed.mvDir = OUTPUT;
    RedLed.mvPinNumber = REDLED;
    RedLed.mvPortNumber = PORTF;
    RedLed.mvPullMode = PULLUP;

    mInit(RedLed);
}

void mOnRed()
{
    mOn(RedLed);
}

void mOffRed()
{
    mOff(RedLed);
}


void mInitBlue()
{
    BlueLed.mvDir = OUTPUT;
    BlueLed.mvPinNumber = BLUELED;
    BlueLed.mvPortNumber = PORTF;
    BlueLed.mvPullMode = PULLUP;

    mInit(BlueLed);
}

void mOnBlue()
{
    mOn(BlueLed);
}

void mOffBlue()
{
    mOff(BlueLed);
}


void mInitGreen()
{
    GreenLed.mvDir = OUTPUT;
    GreenLed.mvPinNumber = GREENLED;
    GreenLed.mvPortNumber = PORTF;
    GreenLed.mvPullMode = PULLUP;

    mInit(GreenLed);
}

void mOnGreen()
{
    mOn(GreenLed);
}

void mOffGreen()
{
    mOff(GreenLed);
}

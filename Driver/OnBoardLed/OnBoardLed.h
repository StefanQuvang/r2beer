/*
 * OnBoardLed.h
 *
 *  Created on: 30. aug. 2020
 *      Author: stefa
 */

#ifndef SOFTWARE_SYSTEM_LAYER_ONBOARDLED_H_
#define SOFTWARE_SYSTEM_LAYER_ONBOARDLED_H_

void mInitRed();
void mInitBlue();
void mInitGreen();
void mOnRed();
void mOnBlue();
void mOnGreen();
void mOffRed();
void mOffBlue();
void mOffGreen();


#endif /* SOFTWARE_SYSTEM_LAYER_ONBOARDLED_H_ */

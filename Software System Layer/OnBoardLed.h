/*
 * OnBoardLed.h
 *
 *  Created on: 30. aug. 2020
 *      Author: stefa
 */

#ifndef SOFTWARE_SYSTEM_LAYER_ONBOARDLED_H_
#define SOFTWARE_SYSTEM_LAYER_ONBOARDLED_H_

class OnBoardLed
{
public:
    // LED RED PF1
    // LED BLUE PF2
    // LED GREEN PF3
    OnBoardLed();
    virtual ~OnBoardLed();
};

#endif /* SOFTWARE_SYSTEM_LAYER_ONBOARDLED_H_ */

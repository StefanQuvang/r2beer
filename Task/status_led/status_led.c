/*****************************************************************************
* University of Southern Denmark
* Embedded C Programming (ECP)
*
* MODULENAME.: leds.c
*
* PROJECT....: ECP
*
* DESCRIPTION: See module specification file (.h-file).
*
* Change Log:
******************************************************************************
* Date    Id    Change
* YYMMDD
* --------------------
* 050128  KA    Module created.
*
*****************************************************************************/

/***************************** Include files *******************************/
#include "FreeRTOS.h"

#include "status_led.h"
#include "Driver/OnBoardLed.h"
#include "TypeDef.h"

/*****************************    Defines    *******************************/


/*****************************   Constants   *******************************/

/*****************************   Variables   *******************************/

/*****************************   Functions   *******************************/

void status_led_init(void)
/*****************************************************************************
*   Input    :  -
*   Output   :  -
*   Function :
*****************************************************************************/
{
    mInitGreen();
}


void status_led_task(void *pvParameters)
{

    status_led_init();

    while(1)
    {
        // Toggle status led
        mOffGreen();
        vTaskDelay(500 / portTICK_RATE_MS); // wait 100 ms.
    }
}


/****************************** End Of Module *******************************/





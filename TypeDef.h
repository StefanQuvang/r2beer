/*
 * TypeDef.h
 *
 *  Created on: 30. aug. 2020
 *      Author: stefa
 */

#ifndef TYPEDEF_H_
#define TYPEDEF_H_

#include <stdbool.h>
#include <HAL/TM4C123G/Processor/tm4c123gh6pm.h>

#define DIGITAL true
#define ANALOG false
#define INPUT false
#define OUTPUT true
#define PULLUP      0x01
#define PULLDOWN    0x02
#define OPENDRAIN   0x03

#define REDLED 0x02
#define BLUELED 0x04
#define GREENLED 0x08

#define PORTF SYSCTL_RCGC2_GPIOF

typedef unsigned char           BOOLEAN;
typedef unsigned char           uint8;     /* Unsigned  8 bit quantity              */
typedef signed   char           int8;     /* Signed    8 bit quantity              */
typedef unsigned short          uint16;    /* Unsigned 16 bit quantity              */
typedef signed   short          int16;    /* Signed   16 bit quantity              */
typedef unsigned long           uint32;    /* Unsigned 32 bit quantity              */
typedef signed   long           int32;    /* Signed   32 bit quantity              */
typedef unsigned long long      uint64;    /* Unsigned 64 bit quantity              */
typedef signed   long long      int64;    /* Signed   64 bit quantity              */
typedef float                   fp32;      /* Single precision floating point       */
typedef double                  fp64;      /* Double precision floating point       */


#endif /* TYPEDEF_H_ */

@startuml
main.cpp -> tm4c123g.cpp : mGetInstance()
main.cpp <- tm4c123g.cpp : tm4c123g* Instance


main.cpp -> tm4c123g.cpp : mGetGpio()
main.cpp <- tm4c123g.cpp : GPIO*


main.cpp -> gpio.cpp : Set config struct
main.cpp -> gpio.cpp : Init()
group Read()
main.cpp -> gpio.cpp : Read()
main.cpp <- gpio.cpp : data
end
group Write()
main.cpp -> gpio.cpp : Write()
end
@enduml
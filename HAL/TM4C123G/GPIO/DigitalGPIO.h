/*
 * DigitalGPIO.h
 *
 *  Created on: 31. aug. 2020
 *      Author: stefa
 */

#ifndef HAL_TM4C123G_GPIO_DIGITALGPIOH_
#define HAL_TM4C123G_GPIO_DIGITALGPIOH_

#include "TypeDef.h"

typedef struct {
    uint8 mvPinNumber;
    uint8 mvPortNumber;
    bool mvDir;
    uint8 mvPullMode;
}Config;

void mInit(Config config);
void mOn(Config config);
void mOff(Config config);



#endif /* HAL_TM4C123G_GPIO_DIGITALGPIOH_ */

/*
 * DigitalGPIO.cpp
 *
 *  Created on: 31. aug. 2020
 *      Author: stefa
 */

#include <HAL/TM4C123G/GPIO/DigitalGPIO.h>
#include "TypeDef.h"

void mInit(Config config)
{
    uint8 dummy;

    SYSCTL_RCGCGPIO_R |= config.mvPortNumber;

    dummy = SYSCTL_RCGCGPIO_R;

    GPIO_PORTF_DIR_R |= config.mvPinNumber;

    // TODO Add Pin Power configuration

    switch (config.mvPortNumber)
    {
        case PORTF:
            if (config.mvPullMode == PULLUP)
            {
                GPIO_PORTF_PUR_R |= config.mvPinNumber;
            }
        break;
    }

    GPIO_PORTF_DEN_R |= config.mvPinNumber;
}

void mOn(Config config)
{
    GPIO_PORTF_DATA_R |= config.mvPinNumber;
}

void mOff(Config config)
{
    GPIO_PORTF_DATA_R ^= config.mvPinNumber;
}
